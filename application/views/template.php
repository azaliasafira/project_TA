<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<title>Pizza</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Modern Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
 <!-- Bootstrap Core CSS -->
<link href="<?=base_url()?>asset/css/bootstrap.min.css" rel='stylesheet' type='text/css' />
<!-- Custom CSS -->
<link href="<?=base_url()?>asset/css/style.css" rel='stylesheet' type='text/css' />
<!-- Graph CSS -->
<link href="<?=base_url()?>asset/css/lines.css" rel='stylesheet' type='text/css' />
<link href="<?=base_url()?>asset/css/font-awesome.css" rel="stylesheet"> 
<!-- jQuery -->
<script src="<?=base_url()?>asset/js/jquery.min.js"></script>
<!----webfonts--->
<link href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900' rel='stylesheet' type='text/css'>
<!---//webfonts--->  
<!-- Nav CSS -->
<link href="<?=base_url()?>asset/css/custom.css" rel="stylesheet">
<!-- Metis Menu Plugin JavaScript -->
<script src="<?=base_url()?>asset/js/metisMenu.min.js"></script>
<script src="<?=base_url()?>asset/js/custom.js"></script>
<!-- Graph JavaScript -->
<script src="<?=base_url()?>asset/js/d3.v3.js"></script>
<script src="<?=base_url()?>asset/js/rickshaw.js"></script>
</head>
<body>
<div id="wrapper">
     <!-- Navigation -->
        <nav class="top1 navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?=base_url()?>asset/index.html">Pizza</a>
            </div>
            <!-- /.navbar-header -->
            <ul class="nav navbar-nav navbar-right">
			    <li class="dropdown">
	        		 <li class="dropdown">
                    <a href="#" class="dropdown-toggle avatar" data-toggle="dropdown"><img src="<?=base_url()?>/asset/images/1.png"><span class="badge">9</span></a>
                    <ul class="dropdown-menu">
                        
                        <li class="m_2"><a href="<?=base_url('index.php/login/logout')?>"><i class="fa fa-lock"></i> Logout</a></li>  
                    </ul>
                </li>
	      		</li>
			</ul>
			
            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                      <!--   <?php if ($this->session->userdata('level') == 'kasir') { ?> -->
                        <!-- <?php }
                            if ($this->session->userdata('level') == 'admin') { ?> -->
                        <li>
                            <a href="<?=base_url('index.php/pizza')?>"><i class="fa fa-laptop nav_icon"></i>Dashboard</a>
                        </li>
                        <li>
                            <a href="<?=base_url('index.php/jenis')?>"><i class="fa fa-indent nav_icon"></i>Jenis Pizza</a>
                        </li>
                        <li>
                            <a href="<?=base_url('index.php/pizza2')?>"><i class="fa fa-envelope nav_icon"></i>Daftar Pizza</a>
                        </li>
                        <li>
                            <a href="<?=base_url('index.php/user')?>"><i class="fa fa-envelope nav_icon"></i>Daftar User</a>
                        </li>
                        <li>
                            <a href="<?=base_url('index.php/transaksi')?>"><i class="fa fa-envelope nav_icon"></i>Transaksi Pembelian</a>
                        </li>
                        <li>
                            <a href="<?=base_url('index.php/history')?>"><i class="fa fa-envelope nav_icon"></i>History Pembelian</a>
                        </li>
                       <!--  <?php }?> -->
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>
        <div id="page-wrapper">
            <?=$konten?>
       </div>
      <!-- /#page-wrapper -->
   </div>
    <!-- /#wrapper -->
    <!-- Bootstrap Core JavaScript -->
    <script src="<?=base_url()?>asset/js/bootstrap.min.js"></script>
</body>
</html>
