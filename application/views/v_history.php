<h2 style="padding: 20px;">Daftar Pesanan</h2>
<?php if ($this->session->flashdata('pesan')): ?>
	<div class="alert alert-success">
		<?= $this->session->flashdata('pesan');?>
	</div>
<?php endif ?>

<table class="table table-hover table-striped" id="example">
	<thead>
	<tr>
		<td>NO</td><td>NO NOTA</td><td>NAMA PEMBELI</td><td>GRAND TOTAL</td>
	</tr>
	</thead>
	<tbody>
		<?php
			$no=0;
			foreach ($daftar_pesanan as $pesan): 
				$no++;?>
		<tr>
		<td><?=$no?></td>
		<td><?=$pesan->id_nota?></td>
		<td><?=$pesan->nama_pembeli?></td>
		<td align="right"><?= number_format($pesan->grand_total)?></td>
		</tr>
		<?php endforeach ?>
	</tbody>
</table>
<?php
			$no=0;
			foreach ($daftar_pesanan as $pesan): 
				$no++;?>

		
	<?php endforeach ?>	
<script type="text/javascript">
  $(document).ready(function(){
    $('#example').DataTable();
  });
 
</script>