<?php //echo var_dump($nota);?> 
<h2 align="center">Nota Pembelian Pizza</h2>
No Nota : <?= $nota->id_nota?><br>
Tanggal Membeli : <?= $nota->tgl_membeli?><br>
Pembeli : <?= $nota->nama_pembeli?>

<table border="1" style="border-collapse: collapse;">
	<tr>
		<th>NO</th><th>Nama Pizza</th><th>Harga</th><th>QTY</th><th>Subtotal</th>
	</tr>
	<?php $no=0; foreach ($detail_nota as $pizza): $no++;?>
	<tr>
		<th><?=$no?></th><th><?=$pizza->nama_pizza?></th><th><?= number_format($pizza->harga)?></th><th><?=$pizza->jumlah?></th><th><?= number_format(($pizza->harga*$pizza->jumlah))?></th>
	</tr>
<?php endforeach ?>
	<tr>
		<th colspan="4">Grand Total</th><th><?= number_format($nota->grand_total)?></th>
	</tr>
</table>

<script type="text/javascript">
	window.print();
	location.href="<?=base_url('index.php/transaksi')?>";
</script>