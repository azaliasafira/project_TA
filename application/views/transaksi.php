	
<?php if($this->session->flashdata('pesan')):?>
<div class="alert alert-warning"><?= $this->session->flashdata('pesan');?></div>


<?php endif ?>
	<div class="row">



<div class="col-md-7">

	<h2>Stok</h2>

<table id="Display" class="table table-hover table-striped">
	<thead>
		<tr>
			<td>no</td><td>Nama Pizza</td><td>Jenis Pizza</td><td>Gambar</td><td>Harga</td><td>Stock</td><td></td>

		</tr>

	</thead>

	<tbody>

		<?php $no = 0; foreach($ambilpizza as $pizza): $no++;?>

		<tr>
			<td><?=$no?></td> 
	        <td><?=$pizza->nama_pizza?></td>
	        <td><?=$pizza->nama_kategori?></td>
	        <td><img src="<?=base_url('asset/gambar/'.$pizza->gambar_pizza)?>" style="width: 40px;"></td>
	        <td><?=$pizza->harga?></td>
	        <td><?=$pizza->stock?></td>
	        <td><a href="<?=base_url('index.php/Cart/addcart/'.$pizza->id_pizza)?>" class ="btn btn-danger">Beli</a></td>
		</tr>

	<?php endforeach ?>

	</tbody>

</table>


	</div>


	<div class="col-md-5">

		<form action="<?= base_url('index.php/Transaksi/bayar')?>" method="post">

<h2>Pembelian</h2>

	

	<table class="table table-striped table-hover">


		<tr>
			
			<td colspan="2">Nama Pembeli :</td><td colspan="4"><input type="text" name="pembeli"></td>
		</tr>
		
		<tr>
			
			<th>no</th><th>Nama Pizza</th><th>qty</th><th>harga</th><th>subtotal</th><th></th>

		</tr>	
		


		<?php $no=0;foreach ($this->cart->contents() as $cart):$no++;?>

		<input type="hidden" name="id_pizza[]" value="<?=$cart['id']?>">
		<input type="hidden" name="rowid[]" value="<?=$cart['rowid']?>">


		<tr><td><?=$no?></td><td><?=$cart['name']?></td><td><input type="number" name="qty[]" value="<?=$cart['qty']?>" class="form-control" style="width:70px;"></td><td><?=$cart['price']?></td><td><?=number_format($cart['subtotal'])?></td><td>  <a href="<?=base_url('index.php/Cart/hapus/'.$cart['rowid'])?>" class="btn btn-danger">x</a> </td></tr>


	<?php endforeach ?>

		<tr>
			<th> GrandTotal :</th>
			<th colspan="4"> <?= number_format($this->cart->total())?></th>
		</tr>

		<tr>
			<th> Bayar :</th>
			<th colspan="4"> <input type="number" required name="bayar" ></th>
		</tr>

		<tr>
			<th> Kembalian :</th>
			<th colspan="4"> <input type="number" name="Kembalian"  value="<?= $this->session->flashdata('kembalian'); ?>"></th>
		</tr>

		<tr>
			
			<td colspan="2"> <input type="submit" name="cart" value="Update qty" class="btn btn-primary"></td>
			<td colspan="2"> <input type="submit" name="transaksi" value="Bayar" class="btn btn-primary"></td>
			<td colspan="2"> <a href="<?= base_url('index.php/Transaksi/hapussemuacart')?>" class="btn btn-primary">Batal</a></td>



		</tr>





	</table>


</form>

		

	</div>

</div>
<script type="text/javascript">
  $(document).ready(function(){
    $('#example').DataTable();
  });
 
</script>