<h2 style="padding: 20px;">Daftar Menu Pizza</h2>
<?=$this->session->flashdata('pesan');?>
<center><a href="#tambah" data-toggle="modal" class="btn btn-warning">Tambah</a></center>
<table id="daftar_pizza" class="table table-hover table-striped">
	<thead>
		<tr>
			<td>NO</td><td>Nama Pizza</td><td>Deskripsi</td><td>Jenis</td><td>Gambar</td><td>Harga</td><td>Stock</td><td>Aksi</td>
		</tr>
	</thead>
	<tbody>
		<?php $no=0;foreach ($tampil_pizza as $pizza) {
			$no++;?>
			<tr>
				<td><?=$no?></td> 
        <td><?=$pizza->nama_pizza?></td>
         <td><?=$pizza->deskripsi?></td>
        <td><?=$pizza->nama_kategori?></td>
        <td><img src="<?=base_url('asset/gambar/'.$pizza->gambar_pizza)?>" style="width: 40px;"></td>
        <td><?=$pizza->harga?></td>
        <td><?=$pizza->stock?></td>
        <td><a href="#edit" onclick="edit('<?=$pizza->id_pizza?>')" data-toggle="modal" class="btn btn-succes">Ubah</a><a href="<?=base_url('index.php/pizza2/hapus/'.$pizza->id_pizza)?>" onclick="return confirm('Apakah Anda Yakin?') class="btn btn-danger">Hapus</a></td>
			</tr>
<?php }?>
	</tbody>
</table>

<div class="modal fade" id="tambah">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title">Tambah Pizza</h4>
      </div>
      <div class="modal-body">
       <form action="<?=base_url('index.php/pizza2/tambah')?>" method="post" enctype="multipart/form-data">
      	<table>
      		<tr>
      			<td>Nama Pizza</td><td><input type="text" name="nama_pizza" class="form-control"></td>
      		</tr>
          <tr>
            <td>Deskripsi</td><td><textarea name="deskripsi" class="form-control"></textarea></td>
          </tr>
          <tr>
            <td>Jenis</td><td><select name="kategori" class="form-control">
              <option></option>
              <?php foreach ($kategori as $kat): ?>
                <option value="<?=$kat->id_kategori?>">
                  <?=$kat->nama_kategori?></option>
              <?php endforeach ?>
            </select></td>
          </tr>
          <tr>
            <td>Gambar</td><td><input type="file" name="gambar" class="form-control"></td>
          </tr>
      		<tr>
      			<td>Harga</td><td><input type="number" name="harga" class="form-control"></td>
      		</tr>
          <tr>
            <td>Stok</td><td><input type="number" name="stock" class="form-control"></td>
          </tr>
      	</table>
      	<input type="submit" name="simpan" value="Simpan" class="btn btn-success">
</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="edit">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title">Ubah Pizza</h4>
      </div>
      <div class="modal-body">
       <form action="<?=base_url('index.php/pizza2/pizza_update')?>" method="post" enctype="multipart/form-data">
        <table>
          <tr>
            <td>ID</td><td><input type="text" name="id_pizza" id="id_pizza" class="form-control"></td>
          </tr>
          <tr>
            <td>Nama Pizza</td><td><input type="text" name="nama_pizza" id="nama_pizza" class="form-control"></td>
          </tr>
          <tr>
            <td>Deskripsi</td><td><input type="text" name="deskripsi" id="deskripsi" class="form-control"></td>
          </tr>
           <tr>
            <td>Jenis</td><td><select name="kategori" id="kategori" class="form-control">
              <option></option>
              <?php foreach ($kategori as $kat): ?>
                <option value="<?=$kat->id_kategori?>">
                  <?=$kat->nama_kategori?></option>
              <?php endforeach ?>
            </select></td>
          </tr>
          <tr>
            <td>Gambar</td><td><input type="file" name="gambar" id="gambar" class="form-control"></td>
          </tr>
          <tr>
            <td>Harga</td><td><input type="number" name="harga" id="harga" class="form-control"></td>
          </tr>
          <tr>
            <td>Stock</td><td><input type="number" name="stock" id="stock" class="form-control"></td>
          </tr>
        </table>
        <input type="submit" name="simpan" value="Simpan" class="btn btn-success">
</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
  $(document).ready(function(){
    // $('#example').DataTable();
  });
  function edit(a){
      $.ajax({
       type:"post",
       url:"<?=base_url()?>index.php/pizza2/edit_pizza/"+a, 
       dataType:"json",
       success:function(data){
        // console.log(data);return false;
        $("#nama_pizza").val(data.nama_pizza);
        $("#deskripsi").val(data.deskripsi);
        $("#kategori").val(data.id_kategori);

        // $("#gambar").val(data.gambar_pizza);
        $("#harga").val(data.harga);
        $("#stock").val(data.stock);
        $("#id_pizza").val(data.id_pizza);
         document.getElementById('gambar').value= data.gambar_pizza;
      }
      });
    }
</script>