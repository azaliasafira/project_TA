<h2 style="padding: 20px;">User</h2>
<?=$this->session->flashdata('pesan');?>
<center><a href="#tambah" data-toggle="modal" class="btn btn-warning">Tambah</a></center>
<table id="example" class="table table-hover table-striped">
	<thead>
		<tr>
			<td>NO</td><td>id_user</td><td>Username</td><td>Password</td><td>Aksi</td>
		</tr>
	</thead>
	<tbody>
		<?php $no=0;foreach ($tampil_user as $user): 
		$no++;?>
		<tr>
			<td><?=$no?></td>
      <td><?=$user->id_user?></td>
      <td><?=$user->username?></td>
      <td><?=$user->password?></td>
      <td><a href="#edit" onclick="edit('<?=$user->id_user?>')" data-toggle="modal" class="btn btn-success">Ubah</a> 
        <a href="<?=base_url('index.php/user/hapus/'.$user->id_user)?>" onclick="return confirm('Apakah Anda Yakin?')" class="btn btn-danger">Hapus</a>
      </td>
		</tr>
		<?php endforeach ?>
		
	</tbody>
</table>
<div class="modal fade" id="tambah">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title">Tambah User</h4>
      </div>
      <div class="modal-body">
       <form action="<?=base_url('index.php/user/tambah')?>" method="post" name="simpan">
      	<table>
          <tr>
            <td>ID User</td><td><input type="text" name="id_user" required class="form-control"></td>
          </tr>
      		<tr>
      			<td>Username</td><td><input type="text" name="username" required class="form-control"></td>
      		</tr>
          <tr>
            <td>Password</td><td><input type="text" name="password" required class="form-control"></td>
          </tr>
      	</table>
      	<input type="submit" name="simpan" value="Simpan" class="btn btn-success">
</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="edit">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title">Edit User</h4>
      </div>
      <div class="modal-body">
       <form action="<?=base_url('index.php/user/user_update')?>" method="post">
        <input type="hidden" name="id_user_lama" id="id_user_lama">
        <table>
          <tr>
            <td>ID User</td><td><input type="text" name="id_user" id="id_user" required class="form-control"></td>
          </tr>
          <tr>
            <td>Username</td><td><input type="text" id="username" name="username" required class="form-control"></td>
          </tr>
          <tr>
            <td>Password</td><td><input type="text" id="password" name="password" required class="form-control"></td>
          </tr>
        </table>
        <input type="submit" name="edit" value="Simpan" class="btn btn-success">
</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button></div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
  $(document).ready(function(){
    $('#example').DataTable();
  });
  function edit(a){
      $.ajax({
       type:"post",
       url:"<?=base_url()?>index.php/user/edit_user/"+a, 
       dataType:"json",
       success:function(data){
        $("#id_user").val(data.id_user);
        $("#username").val(data.username);
        $("#password").val(data.password);
        $("#id_user_lama").val(data.id_user);
      }
      });
    }
</script>