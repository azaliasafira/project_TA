<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jenis extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('m_jenis','jen');
	}

	public function index()
	{
		$data['tampil_jenis']=$this->jen->tampil_jen();
		$data['judul']="Jenis";
		$data['konten']=$this->load->view('v_jenis',$data,TRUE);
		$this->load->view('template',$data);
	}

	public function tambah()
	{
		if ($this->input->post('simpan')) {
			if ($this->jen->simpan_jen()) {
				$this->session->set_flashdata('pesan', 'Menambah Jenis');
				redirect('jenis','refresh');
			} else{
				$this->session->set_flashdata('pesan', 'Gagal Menambah Jenis');
				redirect('jenis','refresh');
			}
		}
	}

	public function edit_jenis($id)
	{
		$data=$this->jen->detail($id);
		echo json_encode($data);
	}

	public function jenis_update()
	{
		if($this->input->post('edit')){

			if($this->jen->edit_jen()){
				$this->session->set_flashdata('pesan', 'Sukses Update Kategori');
				redirect('jenis','refresh');
			} else{
				$this->session->set_flashdata('pesan', 'Gagal Update Kategori');
				redirect('jenis','refresh');
			}
		}
	}

	public function hapus($id='')
	{
		if ($this->jen->cek_jen($id)>0) {
			$this->session->set_flashdata('pesan', 'Maaf Data Digunakan');
			redirect('jenis','refresh');
		} else{
			if ($this->jen->hapus_jen($id)) {
			$this->session->set_flashdata('pesan', 'Sukses Hapus Jenis');
			redirect('jenis','refresh');
			} else{
			$this->session->set_flashdata('pesan', 'Gagal Hapus Jenis');
			redirect('jenis','refresh');
			}
		}
	}
}
