<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function index()
	{
		// if ($this->session->userdata('login')==TRUE) {
		// 	redirect('pizza','refresh');
		// }else{
			$this->load->view('login');		
		// }
	}

	public function proses_login()
		{
			if($this->input->post('login')){
				//validasi
				$this->form_validation->set_rules('username', 'Username', 'trim|required');
				$this->form_validation->set_rules('password', 'Password', 'trim|required');
				if ($this->form_validation->run() == TRUE) 
				{
					$this->load->model('m_user');
					if($this->m_user->get_login()->num_rows()>0)
					{
						$data=$this->m_user->get_login()->row();
						$array = array(
							'login' => TRUE,
							'username' => $data->username,
							'password' => $data->password,
							'level' => $data->level
						);
						
						$this->session->set_userdata( $array );
						redirect('pizza','refresh');
						
					} else{
						$this->session->set_flashdata('pesan', 'Salah Username dan Password');
						redirect('login','refresh');
					
					}
				} else {
					$this->session->set_flashdata('pesan', validation_errors());
					redirect('login','refresh');
				
				}
			}
		}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect('login ','refresh');
	}
}
