

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cart extends CI_Controller {


	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_cart','mct');
	}

	public function index()
	{


		
	}



	public function addcart($id_pizza){


		$cek=$this->mct->cek($id_pizza); 

		if ( $cek == 0){


			$this->session->set_flashdata('pesan', 'Barang Habis Silahkan Hubungi admin');



		 }else{

		$detail = $this->mct->detailpizza($id_pizza);
		$data = array(
			'id'      => $detail->id_pizza,
			'qty'     => 1,
			'price'   => $detail->harga,
			'name'    => $detail->nama_pizza,
			'options' => array()
		);
		
		$this->cart->insert($data);
		
		}

		redirect( base_url('index.php/Transaksi'),'refresh');

	}



	public function hapus($rowid){


			$data = array(
				'rowid' => $rowid,
				'qty'   => 0
			);
			
			$this->cart->update($data);

			redirect( base_url('index.php/Transaksi'),'refresh');
	}

}

/* End of file Cart.php */
/* Location: ./application/controllers/Cart.php */

?>