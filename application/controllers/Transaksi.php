

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaksi extends CI_Controller {


	public function __construct()
	{
		parent::__construct();
		
		$this->load->model('M_pizza','pizza');
		$this->load->model('M_transaksi','mts');
	}

	public function index()
	{
		$data['ambilpizza']=$this->pizza->tampil_pizza();
		$data['konten']=$this->load->view('transaksi',$data,TRUE);
		
		$this->load->view('template', $data);
		
	}


	public function bayar(){


		if($this->input->post('cart')){


				for($i=0;$i <count($this->cart->contents());$i++){


					$data = array(
						'rowid' => $this->input->post('rowid')[$i],
						'qty'   => $this->input->post('qty')[$i],
					);
					
					$this->cart->update($data);

				}

				redirect('Transaksi','refresh');



		}else{


			$cek=$this->mts->check();


			if($cek == 1){

				$status = $this->mts->simpancrt();
				if($status>0){

				$bayar = $this->input->post('bayar');
				$total = $this->cart->total();

				$kembalian = $bayar - $total;

				$this->session->set_flashdata('pesan', 'sukses beli');

				$this->session->set_flashdata('kembalian', $kembalian);

				
				$this->mts->simpannota($status);
				$data['nota']=$this->mts->detail_nota($status);
				$data['detail_nota']=$this->mts->detail_pembelian($status) ;
				$this->cart->destroy();
				$this->load->view('cetak_nota', $data, FALSE);

				}else{

				$this->session->set_flashdata('pesan', 'gagal simpan');
				redirect('Transaksi','refresh');

				}


			}else{

				$this->session->set_flashdata('pesan', 'Stok Kurang ');
				redirect('Transaksi','refresh');
			}

			

		}


	}


	public function hapussemuacart(){


		$this->cart->destroy();
		redirect('transaksi','refresh');


	}









}

/* End of file Transaksi.php */
/* Location: ./application/controllers/Transaksi.php */


?>