<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pizza2 extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('login')!=TRUE) {
			redirect('login','refresh');
		}
		$this->load->model('m_pizza','pizza');
	}

	public function index()
	{
		$data['tampil_pizza']=$this->pizza->tampil_pizza();
		$data['kategori']=$this->pizza->data_kategori();
		$data['judul']="Daftar Pizza";
		$data['konten']=$this->load->view('pizza',$data,TRUE);
		$this->load->view('template',$data);
	}

	public function tambah()
	{
		$this->form_validation->set_rules('nama_pizza', 'Nama Pizza', 'trim|required');
		$this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|required');
		$this->form_validation->set_rules('kategori', 'Kategori', 'trim|required');
		$this->form_validation->set_rules('harga', 'Harga', 'trim|required');
		$this->form_validation->set_rules('stock', 'Stock', 'trim|required');
		if ($this->form_validation->run() == TRUE) {
			$config['upload_path'] = './asset/gambar/';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size']  = '1000';
			$config['max_width']  = '5024';
			$config['max_height']  = '5768';
			if($_FILES['gambar']['name']!=""){
				$this->load->library('upload', $config);
			
				if ( ! $this->upload->do_upload('gambar')){
					$this->session->set_flashdata('pesan', $this->upload->display_errors());
					redirect('pizza2','refresh');
					
				}
				else{
					if($this->pizza->simpan_pizza($this->upload->data('file_name'))){
						$this->session->set_flashdata('pesan', 'sukses menambah');	
					} else {
						$this->session->set_flashdata('pesan', 'gagal menambah');	
					}
					redirect('pizza2','refresh');	
						
				}
			} else {
				if($this->pizza->simpan_pizza('')){
					$this->session->set_flashdata('pesan', 'sukses menambah');	
				} else {
					$this->session->set_flashdata('pesan', 'gagal menambah');	
				}
				redirect('pizza2','refresh');
				
			}
			
		} else {
			$this->session->set_flashdata('pesan', validation_errors());
			redirect('pizza2','refresh');
			
		}

	}
	//mengeluarkan data di modal
	public function edit_pizza($id)
	{
		
		$data=$this->pizza->detail($id);
		// echo var_dump($data) ;exit;
		echo json_encode($data);
	}

	public function pizza_update()
	{

		if($this->input->post('simpan')){
			if($_FILES['gambar']['name']==""){
				if($this->pizza->pizza_update_no_foto()){
					$this->session->set_flashdata('pesan', 'Sukses update');
					redirect('pizza2');
				} else {
					$this->session->set_flashdata('pesan', 'Gagal update');
					redirect('pizza2');
				}
			} else {
				$config['upload_path'] = './asset/gambar/';
				$config['allowed_types'] = 'gif|jpg|png|jpeg';
				$config['max_size']  = '20000';
				$config['max_width']  = '5024';
				$config['max_height']  = '5768';
				
				$this->load->library('upload', $config);
				
				if ( ! $this->upload->do_upload('gambar')){
					$this->session->set_flashdata('pesan', 'Gagal Upload');
					redirect('pizza2');
				}
				else{
					if($this->pizza->pizza_update_dengan_foto($this->upload->data('file_name'))){
						$this->session->set_flashdata('pesan', 'Sukses update');
						redirect('pizza2');
					} else {
						$this->session->set_flashdata('pesan', 'Gagal update');
						redirect('pizza2');
					}
				}
			}
			
		}

	}

	public function hapus($id_pizza='')
	{
		if ($this->pizza->hapus_pizza($id_pizza)) {
			$this->session->set_flashdata('pesan', 'Sukses Hapus Pizza');
			redirect('pizza2', 'refresh');
		} else{
			$this->session->set_flashdata('pesan', 'Gagal Hapus pizza');
			redirect('pizza2', 'refresh');
		}
	}
}