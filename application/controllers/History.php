<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class History extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_history','history');
	}
	public function index()
	{
		$data['judul']="daftar pesanan";
		$data['daftar_pesanan']=$this->history->tm_pesanan();
		$data['konten']=$this->load->view('v_history', $data, TRUE);
		$this->load->view('template', $data, FALSE);
	}
}
