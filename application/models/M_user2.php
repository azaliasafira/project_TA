<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_user2 extends CI_Model {


		public function tampil_user()
		{
			$tm_user=$this->db->get('user')->result();
			return $tm_user;
		}
		
		public function simpan_user()
		{
			$object=array(
			'id_user'=>$this->input->post('id_user'),
			'username'=>$this->input->post('username'),
			'password'=>$this->input->post('password')
			);
			return $this->db->insert('user', $object);
		}

		public function detail($a)
		{
			return $this->db->where('id_user',$a)
						->get('user')
						->row();
		}

		public function edit_user()
		{
			$object=array(
				'id_user'=>$this->input->post('id_user'),
				'username'=>$this->input->post('username'),
				'password'=>$this->input->post('password')
			);
			return $this->db->where('id_user',$this->input->post('id_user_lama'))->update('user',$object);
		}

		public function hapus_user($id='')
		{
			return $this->db->where('id_user',$id)->delete('user');
		}

		public function cek_user($id)
		{
			return $this->db->where('id_user',$id)
						->get('user')
						->num_rows();
		}
}