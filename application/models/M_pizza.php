<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_pizza extends CI_Model {

	public function tampil_pizza()
	{
		$tm_pizza=$this->db
					  ->join('kategori','kategori.id_kategori=pizza.id_kategori')
					  ->get('pizza')->result();
		return $tm_pizza;
	}

	public function data_kategori()
	{
		return $this->db->get('kategori')->result();
	}
		
	public function simpan_pizza($nama_file)
	{
		if ($nama_file=="") {
			$object=array(
			'nama_pizza'=>$this->input->post('nama_pizza'),
			'deskripsi'=>$this->input->post('deskripsi'),
			'id_kategori'=>$this->input->post('kategori'),
			'harga'=>$this->input->post('harga'),
			'stock'=>$this->input->post('stock')
			);
		} else{
			$object=array(
			'nama_pizza'=>$this->input->post('nama_pizza'),
			'deskripsi'=>$this->input->post('deskripsi'),
			'id_kategori'=>$this->input->post('kategori'),
			'harga'=>$this->input->post('harga'),
			'stock'=>$this->input->post('stock'),
			'gambar_pizza'=>$nama_file
			);
			return $this->db->insert('pizza',$object);
		}
	}

	public function detail($id)
	{
		$tm_pizza=$this->db
					  ->join('kategori','kategori.id_kategori=pizza.id_kategori')
					  ->where('id_pizza',$id)
					  ->get('pizza')
					  ->row();
		// return $this->db->last_query();return false;
		return $tm_pizza;
	}

	public function pizza_update_no_foto()
	{
		
		$object=array(
			'nama_pizza'=>$this->input->post('nama_pizza'),
			'deskripsi'=>$this->input->post('deskripsi'),
			'id_kategori'=>$this->input->post('kategori'),
			'harga'=>$this->input->post('harga'),
			'stock'=>$this->input->post('stock')
			);
			return $this->db->where('id_pizza',$this->input->post('id_pizza'))
				->update('pizza', $object);
				// $this->db->last_query();
	}

	public function hapus_pizza($id_pizza='')
	{
		return $this->db->where('id_pizza', $id_pizza)->delete('pizza');
	}

	public function pizza_update_dengan_foto($nama_file)
	{
		$object=array(
			'nama_pizza'=>$this->input->post('nama_pizza'),
			'deskripsi'=>$this->input->post('deskripsi'),
			'id_kategori'=>$this->input->post('kategori'),
			'harga'=>$this->input->post('harga'),
			'stock'=>$this->input->post('stock'),
			'gambar_pizza'=>$nama_file
		);
			return $this->db->where('id_pizza',$this->input->post('id_pizza'))
				->update('pizza', $object);
	}


}