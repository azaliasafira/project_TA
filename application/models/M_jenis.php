<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_jenis extends CI_Model {


		public function tampil_jen()
		{
			$tm_jenis=$this->db->get('kategori')->result();
			return $tm_jenis;
		}
		
		public function simpan_jen()
		{
			$object=array(
			'id_kategori'=>$this->input->post('id_kategori'),
			'nama_kategori'=>$this->input->post('nama_kategori'),
			);
			return $this->db->insert('kategori', $object);
		}

		public function detail($a)
		{
			return $this->db->where('id_kategori',$a)
						->get('kategori')
						->row();
		}

		public function edit_jen()
		{
			$object=array(
				'id_kategori'=>$this->input->post('id_kategori'),
				'nama_kategori'=>$this->input->post('nama_kategori')
			);
			return $this->db->where('id_kategori',$this->input->post('id_kategori_lama'))->update('kategori',$object);
		}

		public function hapus_jen($id='')
		{
			return $this->db->where('id_kategori',$id)->delete('kategori');
		}

		public function cek_jen($id)
		{
			return $this->db->where('id_kategori',$id)
						->get('pizza')
						->num_rows();
		}
}