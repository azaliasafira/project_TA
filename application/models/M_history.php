<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_history extends CI_Model {

	public function tm_pesanan()
	{
		return $this->db
					->get('nota')->result();
	}
	public function detail_pesanan($id)
	{
		return $this->db->where('id_nota',$id)->get('nota')->row();
	}
	public function trans($id_nota)
	{
		return $this->db->where('nota.id_nota',$id_nota)
					->join('pizza','pizza.id_pizza=transaksi.id_pizza')
					->join('kategori','kategori.id_kategori=pizza.id_kategori')
					->join('nota','transaksi.id_nota=nota.id_nota')
					->get('transaksi')->result();
	}
}